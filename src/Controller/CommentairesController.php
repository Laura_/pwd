<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile/comment")
 */
class CommentairesController extends AbstractController
{
    /**
     * @Route("/", name="commentaires_index", methods={"GET"})
     */
    public function index(CommentairesRepository $commentairesRepository): Response
    {
        return $this->render('commentaires/index.html.twig', [
            'commentaires' => $commentairesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new_feedback", methods={"POST"})
     */
    public function new(UserRepository $userRepository, ArticleRepository $articleRepository, Request $request): Response
    {
        $commentaire = new Commentaires();

        $commentaire->setNote($request->request->get('note'));
        $commentaire->setContenu($request->request->get('contenu'));
        $commentaire->setArticle($articleRepository->findById($request->request->get('article'))[0]);
        $commentaire->setuser($userRepository->findById($request->request->get('user'))[0]);
        
        $form = $this->createForm(CommentairesType::class, $commentaire);
        $form->handleRequest($request);

        // if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commentaire);
            $entityManager->flush();

            return $this->redirectToRoute('pagelivre', [
                "id" => $request->request->get('article'),
            ]);
        // }

        return $this->redirectToRoute('pagelivre', [
            "id" => $request->request->get('article'),
        ]);
    }

}
