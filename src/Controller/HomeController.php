<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use App\Repository\CommentairesRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Form\ArticleType;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function showAllBooks(ArticleRepository $articleRepository, CategorieRepository $categorieRepository): Response
    {
        $pageSize = 9;
        $articles = $articleRepository->findByNewer();
        $count = sizeof($articles);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'category' => 'Home',
            'articles' => array_splice($articles, 0, $pageSize),
            'count' => $count,
            'pages' => 1,
            'currentPage' => 1,
        ]);
    }

    /**
     * @Route("/pagelivre/{id<\d+>}",name="pagelivre", methods={"GET"})
     */
    public function pageLivre(article $article, CommentairesRepository $CommentairesRepository, Request $request): Response
    {
        $commentaires = $CommentairesRepository->findByArticle($article->getId());

        return $this->render('home/pagelivre.html.twig', [
            'article' => $article,
            'commentaires' => $commentaires,
        ]);
    }

    /**
     * @Route("/list/{category}", name="listCategory", methods={"GET"})
     */
    public function listCategory($category, ArticleRepository $ArticleRepository, CategorieRepository $CategorieRepository)
    {
        $pageSize = 9;
        $result = $ArticleRepository->findByCategory($category);
        $count = sizeof($result);


        if ($count != 0) {
            $pages = ceil($count / $pageSize);
        } else {
            $pages = 0;
        }

        return $this->render('list.html.twig', [
            'controller_name' => 'HomeController',
            'category' => ucfirst($category),
            'articles' => array_splice($result, 0, $pageSize),
            'count' => $count,
            'pages' => $pages,
            'currentPage' => 1,
        ]);
    }

    /**
     * @Route("/list/{category}/{page}", name="listCategoryPage", methods={"GET"})
     */
    public function listCategoryPage($category, $page, ArticleRepository $ArticleRepository, CategorieRepository $CategorieRepository)
    {
        $pageSize = 9;
        $result = $ArticleRepository->findByCategory($category);
        $count = sizeof($result);


        if ($count != 0) {
            $pages = ceil($count / $pageSize);
        } else {
            $pages = 0;
        }

        return $this->render('list.html.twig', [
            'controller_name' => 'HomeController',
            'category' => ucfirst($category),
            'articles' => array_splice($result, ($page - 1) * $pageSize, $pageSize),
            'count' => $count,
            'pages' => $pages,
            'currentPage' => $page,
        ]);
    }
    /**
     * @Route("/listlivre", name="listlivre", methods={"GET"})
     */
    public function affiche(Request $request, ArticleRepository $ArticleRepository): response
    {
        $search = new Article();
        $form = $this->createForm(ArticleType::class, $search);
        $form->handleRequest($request);
        $motcle = $request->get('search');
        $articles=$ArticleRepository->findByTitre($motcle);
        return $this->render('home/search.html.twig', array(
            'search'=>$search,
            'articles'=>$articles
        ));
          
    }
    
    

    /**
     * @Route("/login", name="app_login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }
    
    /**
     * @Route("/posts/{id}", name="posts")
     */
    public function posts($id, CommentairesRepository $CommentairesRepository)
    {
        $commentaires = $CommentairesRepository->findByUser($id);
        
        return $this->render('home/posts.html.twig', [
            'controller_name' => 'HomeController',
            'commentaires' => $commentaires,
        ]);
    }
    /**
     * @Route("/search", name="search")
     */
    public function search()
    {
        return $this->render('home/search.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/register", name="app_register")
     */
    public function register()
    {
        return $this->render('registration/register.html.twig', [
            'controller_name' => 'RegistrationController',
        ]);
    }
    /**
     * @Route("/user", name="user")
     */
    public function user()
    {

        return $this->render('home/user.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
