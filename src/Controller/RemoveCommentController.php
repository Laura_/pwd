<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile/remove")
 */
class RemoveCommentController extends AbstractController
{
    /**
     * @Route("/comment-del/{id}", name="removeComment")
     */
    public function delete(Request $request, Commentaires $commentaire): Response
    {
        $user = $this->getUser();
        //if ($this->isCsrfTokenValid('delete' . $commentaire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($commentaire);
            $entityManager->flush();
        //}

        return $this->redirectToRoute('pagelivre');
    }
}

