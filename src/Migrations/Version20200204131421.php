<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200204131421 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE appreciation');
        $this->addSql('DROP TABLE give_feedback');
        $this->addSql('ALTER TABLE commentaires ADD user_id INT NOT NULL, ADD article_id INT NOT NULL');
        $this->addSql('ALTER TABLE commentaires ADD CONSTRAINT FK_D9BEC0C4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commentaires ADD CONSTRAINT FK_D9BEC0C47294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D9BEC0C4A76ED395 ON commentaires (user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D9BEC0C47294869C ON commentaires (article_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE appreciation (id INT AUTO_INCREMENT NOT NULL, stars INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE give_feedback (id INT AUTO_INCREMENT NOT NULL, commentaire_id INT NOT NULL, article_id INT NOT NULL, user_id INT NOT NULL, UNIQUE INDEX UNIQ_F2B68E0F7294869C (article_id), UNIQUE INDEX UNIQ_F2B68E0FA76ED395 (user_id), UNIQUE INDEX UNIQ_F2B68E0FBA9CD190 (commentaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE give_feedback ADD CONSTRAINT FK_F2B68E0F7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE give_feedback ADD CONSTRAINT FK_F2B68E0FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE give_feedback ADD CONSTRAINT FK_F2B68E0FBA9CD190 FOREIGN KEY (commentaire_id) REFERENCES commentaires (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE commentaires DROP FOREIGN KEY FK_D9BEC0C4A76ED395');
        $this->addSql('ALTER TABLE commentaires DROP FOREIGN KEY FK_D9BEC0C47294869C');
        $this->addSql('DROP INDEX UNIQ_D9BEC0C4A76ED395 ON commentaires');
        $this->addSql('DROP INDEX UNIQ_D9BEC0C47294869C ON commentaires');
        $this->addSql('ALTER TABLE commentaires DROP user_id, DROP article_id');
    }
}
