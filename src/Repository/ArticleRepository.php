<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // Retrieve all books for a specific category
    public function findByCategory($category)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT *
            FROM article a, categorie c
            WHERE a.categorie_id = c.id
            AND c.nom = :category
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'category' => $category,
        ]);

        return $stmt->fetchAll();
    }

    // Retrieve newer added books
    public function findByNewer() {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT *
            FROM article
            ORDER BY id DESC
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
    
    public function findByTitre($motcle){
        $query = $this->createQueryBuilder('a')
                ->Where('a.titre like :titre')
                ->setParameter('titre','%'.$motcle.'%')
                ->orderBy('a.titre','ASC')
                ->getQuery()
                ;
        return $query->getResult();
    }
}
